from django.shortcuts import render

# Create your views here.
from .forms import filform
from djapp.models import users
def disp(request):
    form=filform(request.POST or None,request.FILES or None)
    if form.is_valid():
        cite=request.POST['city']
        if cite=='': filt=users.objects.all()
        else: filt=users.objects.filter(city=cite)
        return render(request,'report.html',{'data':filt,'form':form})
    else:
        dt=users.objects.all()
        return render(request,'report.html',{'data':dt,'form':form})

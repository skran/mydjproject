from django.shortcuts import render
from django.http import HttpResponse
from .models import users
from .forms import myform
# Create your views here.
def fun(request):
    formobj=myform(request.POST or None, request.FILES or None)
    if formobj.is_valid():
        formobj.save()
        x=request.POST['name']
        y=request.POST['city']        
        return render(request,'test.html',{'x':x,'y':y})
    else:
        return render(request,'test.html',{'form':formobj})
